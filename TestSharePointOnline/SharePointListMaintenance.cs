﻿using Microsoft.SharePoint.Client;
using RestSharp;
using RestSharp.Serialization.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Xml.Serialization;
using JT = System.Text.Json;
using System.Linq;
using System.Reflection;
using File = System.IO.File;

namespace TestSharePointOnline
{


    class SharePointListMaintenance : ExploreSharepoint
    {

        private string _ResourceList = "DeptNames.txt";
        private List<string> _DeptNames;

        public SharePointListMaintenance(string tenant) : base(tenant)
        {
            InitDeptNames();
        }

        private void InitDeptNames()
        {
            _DeptNames = new List<string>();
            DirectoryInfo di = new FileInfo(
                Assembly.GetExecutingAssembly().Location).Directory;

            string fp = Path.Combine(di.FullName, "Resources",_ResourceList);

            if (File.Exists(fp))
            {
                _DeptNames = File.ReadAllLines(fp).OrderBy(o => o).ToList() ;
            }

        }



        /// <summary>
        /// Create a new list with columns supplied by the Dictionary
        /// </summary>
        /// <param name="name">Name of the new content type</param>
        /// <param name=""></param>
        /// <see cref="https://www.kofrimpong.com/create-sharepoint-field-types-with-xml-using-pnp-powershell/"/>
        /// <returns>successful</returns>
        public bool CreateList(string listName, string description, Dictionary<string, string> columnDefs)
        {
            ClientContext context = O365LogOn(new Uri(_tenant));
            if (context == null)
                return false;

            //CreateContentType(contentTypeName, description, context);
            List targetList = context.Web.Lists.GetByTitle(listName);
            if (targetList != null)
                return false;

            ListCreationInformation listCreationInformation = new ListCreationInformation();
            listCreationInformation.Title = listName;
            listCreationInformation.Description = listName;
            listCreationInformation.TemplateType = (int)ListTemplateType.GenericList;
            List list = context.Web.Lists.Add(listCreationInformation);
            list.Update();
            context.ExecuteQuery();


            // Add columns 

            foreach (KeyValuePair<string, string> kv in columnDefs)
            {
                string fieldSchema = $"<Field DisplayName ='{kv.Key}' Name='{kv.Key}' Type='{kv.Value}' />";
                Field f = list.Fields.AddFieldAsXml(fieldSchema, true, AddFieldOptions.DefaultValue);
            }


            //FieldNumber fldNumber = context.CastTo<FieldNumber>(f);
            //fldNumber.MaximumValue = 100;
            //fldNumber.MinimumValue = 50;
            //fldNumber.Update();
            context.ExecuteQuery();


            return true;


        }

        internal bool FillList(int numRows, string listName, Dictionary<string, string> columnDefs, out string errorMessage)
        {
            errorMessage = String.Empty;
            Random numGen = new Random();



            int ID_Init = 120;
            DateTime start = new DateTime(2015, 1, 1);
            int range = (DateTime.Today - start).Days;
            ClientContext context = O365LogOn(new Uri(_tenant));
            if (context == null)
                return false;

            //CreateContentType(contentTypeName, description, context);
            List targetList = context.Web.Lists.GetByTitle(listName);
            if (targetList == null)
            {
                errorMessage = $"{listName} does not exists as a SharePoint list.";
                return false;
            }

            for (int i = 0; i < numRows; i++)
            {
                Console.WriteLine($"Creating Item:{i}");
                ID_Init = CreateGenListItem(columnDefs, numGen, ID_Init, start, range, context, targetList);

                if (i % 10 == 0)
                    Console.Clear();
            }

                



            return true;
        }

        private int CreateGenListItem(Dictionary<string, string> columnDefs, Random numGen, int ID_Init, DateTime start, int range, ClientContext context, List targetList)
        {
            ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
            ListItem newItem = targetList.AddItem(itemCreateInfo);
            foreach( var fv in newItem.FieldValuesForEdit.FieldValues)
                Console.WriteLine(fv);

            foreach (KeyValuePair<string, string> kv in columnDefs.Where(q => q.Key != "ID"))
            {
                object val = new object();
                switch (kv.Value)
                {
                    case "Number":
                        val = ID_Init++;
                        break;
                    case "Text":
                        int rndLine = numGen.Next(0, _DeptNames.Count);
                        val = _DeptNames[rndLine];
                        break;
                    case "Currency":
                        val = Convert.ToDecimal(numGen.Next(0, 10000));
                        break;
                    case "DateTime":
                        val = start.AddDays(numGen.Next(range));
                        break;
                    case "Boolean":
                        val = numGen.Next(0, 1000) % 2 == 0;
                        break;
                    default:
                        break;

                }
                newItem[kv.Key] = val;
                newItem.Update();
            }

            context.ExecuteQuery();
            return ID_Init;
        }

        private static void CreateContentType(string contentTypeName, string description, ClientContext context)
        {
            // Cache a reference to the content types collection for the site
            ContentTypeCollection contentTypes = context.Web.ContentTypes;

            // Load (retrieve) the content types collection
            context.Load(contentTypes);
            context.ExecuteQuery();

            ContentType contentType = null;
            // Already exists
            if (!contentTypes.Any(ct => ct.Name == contentTypeName))
                contentType = DefineContentType(contentTypeName, description, context, contentTypes);
            else
                contentType = contentTypes.First(ct => ct.Name == contentTypeName);
        }

        private static ContentType DefineContentType(string name, string description, ClientContext context, ContentTypeCollection contentTypes)
        {
            // Find the template used to define the new content type via inheritance
            ContentType parentType = contentTypes.Where(ct => ct.Name == "Item").FirstOrDefault();
            ContentTypeCreationInformation creationInformation = new ContentTypeCreationInformation();

            // Name, description and the group of the new content type
            creationInformation.Name = name;
            creationInformation.Description = description;
            creationInformation.Group = "Data Models Group";

            // Create the inheritance relationship 
            creationInformation.ParentContentType = parentType;

            // Add the new type to the collection of content types
            ContentType contentType = contentTypes.Add(creationInformation);
            context.ExecuteQuery();

            return contentType;
        }
    }
}

