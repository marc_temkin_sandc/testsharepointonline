﻿using Microsoft.SharePoint.Client;
using RestSharp;
using RestSharp.Serialization.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Xml.Serialization;
using JT = System.Text.Json;
using System.Linq;


namespace TestSharePointOnline
{
    class ExploreSharepoint
    {
        protected SharePointOnlineCredentials _credentials = null;
        protected string _tenant;
        private List<DocRecord> allRecords;

        public ExploreSharepoint(string tenant)
        {
            this._tenant = tenant;
        }

        public bool AddListItem(out string errorMessage)
        {
            errorMessage = String.Empty;

            Uri url = new Uri("https://sandc4.sharepoint.com/sites/MarcTemkinLearningSite");
            try
            {
                using (ClientContext clientContext = O365LogOn(url))
                {
                    List targetList = clientContext.Web.Lists.GetByTitle("Sample");

                    ListItemCreationInformation info = new ListItemCreationInformation();
                    ListItem oItem = targetList.AddItem(info);
                    
                    oItem["Title"] = "A Dog named Zed";
                    oItem["Description"] = "This is a story about a dog named zed";
                    oItem["Age"] = 2;
                    oItem["IsTheStar"] = true;

                    oItem.Update();
                    clientContext.ExecuteQuery();
                }
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                return false;
            }
            return true;
        }

        public ClientContext O365LogOn(Uri url)
        {
            ClientContext clientContext = null;
            ClientContext ctx = null;

            try
            {
                clientContext = new ClientContext(url);
                clientContext.Credentials = _credentials;
                clientContext.ExecuteQuery();
                ctx = clientContext;
            }
            finally
            {
                if (clientContext != null)
                    clientContext.Dispose();
            }
            return ctx;
        }

        public bool Login(string username, out string errorMessage)
        {
            errorMessage = String.Empty;
            Console.WriteLine("Enter pwd:");
            string password = Console.ReadLine();
            var securePassword = new SecureString();
            foreach (var c in password)
                securePassword.AppendChar(c);
            try
            {
                _credentials = new SharePointOnlineCredentials(username, securePassword);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                return false;
            }
            return true;
        }

        internal void UpdateRecord(string base_url, Guid listGuid, int ItemID, Guid updated)
        {
            ClientContext context = new ClientContext(base_url);
            context.Credentials = this._credentials;

/*            Web web = context.Web;

            context.Load(web.Lists,
                lists => lists.Where(li => li.Title == "Documents").
                Include(list => list.Title, List => List.Id));

            context.ExecuteQuery();

            foreach (List list in web.Lists)
            {
                Console.WriteLine($"{list.Title}, {list.Id}");
            }
            
*/            
            List docList = context.Web.Lists.GetById(listGuid);
            ListItem listItem = docList.GetItemById(ItemID);
            
            

            Console.WriteLine(listItem["CategoryDescription"].ToString());
            
            listItem.Update();
            
                
            
            //listItem["Description"] = updated;
            context.ExecuteQuery();
            
        }

        public bool RetrieveListItems(Guid g)
        {
            var client = new RestClient(_tenant); // /Items(9401)
            allRecords = new List<DocRecord>();
            // /_api
            // '82555c6c-d84f-4019-8974-067048308bae'
            
            string request = $"/_api/Web/Lists(guid'{g.ToString()}')/Items";
            while (!String.IsNullOrWhiteSpace(request))
            {
                var getWebTitleRequest = new RestRequest(request, Method.GET);
                getWebTitleRequest.AddHeader("Accept", "application/json");
                client.CookieContainer = new System.Net.CookieContainer();
                client.CookieContainer.SetCookies(new Uri(_tenant),
                    _credentials.GetAuthenticationCookie(new Uri(_tenant)));
                
                JsonDeserializer deserial = new JsonDeserializer();
                var result = deserial.Deserialize<Dictionary<string, string>>
                    (client.Execute(getWebTitleRequest));
                string nextLink = "";
                if (result.ContainsKey("odata.nextLink"))
                    nextLink = result["odata.nextLink"];
                else
                    nextLink = String.Empty;
                if (result.ContainsKey("value"))
                {
                    string docRecord = result["value"];
                    allRecords.AddRange(
                    JT.JsonSerializer.Deserialize<List<DocRecord>>(docRecord));
                }
                request = nextLink;
            }

            return allRecords.Count > 0;

            //allRecords.Select((rec, i) => new { Id = rec.Id, GUID = rec.GUID, position = i }).ToList().
            //    ForEach(rec => Console.WriteLine($"{rec.position},{rec.Id}, {rec.GUID}"));
            //Console.WriteLine(result["Description"]);
        }

        public bool OutputExcel(string fn, out string errorMessage)
        {
            errorMessage = String.Empty;

            ExcelOutput excelOutput = new ExcelOutput();
            try
            {
                if (excelOutput.CreateWorkbook(fn, allRecords))
                    return true;
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                return false;

            }
            return false;
        }

        public bool OutputXML(string fn, out string errorMessage)
        {
            errorMessage = String.Empty;
            XmlSerializer serializer = new XmlSerializer(typeof(List<DocRecord>));
            using (TextWriter writer = new StreamWriter(fn ))
            {
                try
                {
                    serializer.Serialize(writer, allRecords);
                    
                }
                catch (Exception e)
                {
                    errorMessage = e.Message;
                    return false;
                }
            }
            return true;
        }
    }
}
