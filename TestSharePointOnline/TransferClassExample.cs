﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace TestSharePointOnline
{
    class TransferProgram
    {
        static void Main(string[] args)
        {
            string userName = "marc.temkin@sandc.com";
            Console.WriteLine("Enter Your Password Please  ------- ");
            SecureString password = GetPasswordOfYourSite();
            // ClienContext - Get the context for the SharePoint Online Site  


            //DoesNotQuiteWork(userName, password);

        }

        private static void DoesNotQuiteWork(string userName, SecureString password)
        {
            ClientContext contextDestination = new ClientContext(
                "https://sandc4.sharepoint.com/sites/MarcTemkinLearningSite/");

            //use SPFileCollection.Add()'
            List destList = contextDestination.Web.Lists.GetByTitle("Documents");
            ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();

            using (var clientContext = new
            ClientContext("https://sandc4.sharepoint.com/sites/SalesManualReplica/"))
            {
                // SharePoint Online Credentials  
                clientContext.Credentials = new SharePointOnlineCredentials(userName, password);
                contextDestination.Credentials = new SharePointOnlineCredentials(userName, password);
                // Get the SharePoint web  
                Web web = clientContext.Web;
                List lst = web.Lists.GetByTitle("Documents");
                CamlQuery cq = CamlQuery.CreateAllItemsQuery(10);
                ListItemCollection lic = lst.GetItems(cq);
                clientContext.Load(lic);
                // Load the Web properties            
                clientContext.ExecuteQuery();
                foreach (ListItem SourcelistItem in lic)
                {
                    if (SourcelistItem.FileSystemObjectType == FileSystemObjectType.File)
                    {
                        string fileName = SourcelistItem["FileRef"].ToString();



                        // We have all the list item data. For example, Title.      
                        ListItem DestnewItem = destList.AddItem(itemCreateInfo);
                        DestnewItem["Title"] = SourcelistItem["Title"];
                        //DestnewItem["GUID"] = SourcelistItem["GUID"];
                        DestnewItem["Language"] = SourcelistItem["Language"];

                        DestnewItem.Update();
                    }
                }
                contextDestination.ExecuteQuery();
            }
        }

        private static SecureString GetPasswordOfYourSite()
        {
            ConsoleKeyInfo info;
            //Get the user's password as a SecureString  
            SecureString securePassword = new SecureString();
            do
            {
                info = Console.ReadKey(true);
                if (info.Key != ConsoleKey.Enter)
                {
                    securePassword.AppendChar(info.KeyChar);
                }
            }
            while (info.Key != ConsoleKey.Enter);
            return securePassword;
        }
    }
}