﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;
namespace TestSharePointOnline
{
    class Program
    {
        static void Main(string[] args)
        {
            var username = "marc.temkin@sandc.com";
                //"mtemkin44@docslink.onmicrosoft.com";
            string password = String.Empty;
            var tenanthost = "docslink";
            Console.WriteLine("Enter pwd:");
            password = Console.ReadLine();
            var securePassword = new SecureString();
            foreach (var c in password)
                securePassword.AppendChar(c);
            var credentials = new SharePointOnlineCredentials(username, securePassword);
            var tenant = "https://sandc4.sharepoint.com/sites/SampleTopicSite";
                //"https://" + tenanthost + ".sharepoint.com";
            using (var cc = new ClientContext(tenant))
            {
                cc.Credentials = credentials;
                cc.Load(cc.Web, w => w.Title);
                try
                {
                    cc.ExecuteQuery();
                }
                catch (UnauthorizedAccessException ae)
                {
                    Console.WriteLine(ae.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                Console.WriteLine(cc.Web.Title);
            }
        }
    }
}
