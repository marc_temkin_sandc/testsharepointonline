﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.SharePoint.Client;
using RestSharp;
using RestSharp.Deserializers;
using RestSharp.Serialization.Json;
using JT = System.Text.Json;
namespace TestSharePointOnline
{
    class spRest
    {
        static void Main(string[] args)
        {
            var tenant =
                "https://sandc4.sharepoint.com/sites/SalesManualReplica";
            //"https://sandc4.sharepoint.com/sites/sales-manual";
            ExploreSharepoint exploreSharepoint = new ExploreSharepoint(tenant);

            var username = "marc.temkin@sandc.com";
            string errorMessage = String.Empty;
            //if (!exploreSharepoint.Login(username, out errorMessage))
            //{
            //    Console.WriteLine(errorMessage);
            //    return;
            //}

            tenant = "https://sandc4.sharepoint.com/sites/MarcTemkinLearningSite";
            SharePointListMaintenance splm = new SharePointListMaintenance(tenant);

            if (!splm.Login(username, out errorMessage))
            {
                Console.WriteLine(errorMessage);
                return;
            }

            Dictionary<string, string> columnsDict = new Dictionary<string, string>
            {
                { "ID0", "Number" },    // Actual name for AccountID
                { "Title", "Text" },
                { "Name", "Text" },
                { "Amount", "Currency" },
                { "When",  "DateTime"},
                { "Contacted", "Boolean" }
            };
            if (!splm.CreateList("ListAccountTypes","Sample List for Accounts", columnsDict))
            {
                Console.WriteLine("Was not able to create a new list type");
            }
            if (!splm.FillList(1000,"ListAccountTypes", columnsDict, out errorMessage))
            {
                Console.WriteLine($"Was not able to add new list items due to:{errorMessage}");
            }

            //if (!exploreSharepoint.AddListItem(out errorMessage))
            //{
            //    Console.WriteLine(errorMessage);
            //    return;
            //}
            /*
            Guid g = new Guid("82555c6c-d84f-4019-8974-067048308bae");
            int ItemID = 9401;
            string url = tenant;
                //"${tenant}/_api/Web/Lists(guid'{g.ToString()}')/Items({ItemID})";
            Guid updated = new Guid("70cb5be6-1cd5-414b-8d67-8847ef0e0d8b");
            exploreSharepoint.UpdateRecord(url, g, ItemID, updated);
            */
            //errorMessage = RetrieveAndArchiveLists(exploreSharepoint,g);
        }

        private static string RetrieveAndArchiveLists(ExploreSharepoint exploreSharepoint, Guid g)
        {
            string errorMessage = String.Empty;
            if (exploreSharepoint.RetrieveListItems(g))
            {
                if (!exploreSharepoint.OutputExcel("DocRecords2.xlsx", out errorMessage))
                {
                    Console.WriteLine(errorMessage);
                }
                if (!exploreSharepoint.OutputXML("AllRecords.xml", out errorMessage))
                {
                    Console.WriteLine(errorMessage);
                }
            }

            return errorMessage;
        }
    }
}
