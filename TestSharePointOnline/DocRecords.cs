﻿using System;
using System.Xml.Serialization;

namespace TestSharePointOnline
{
    public class Rootobject
    {
        public DocRecord[] records { get; set; }
    }
    [Serializable]
    [XmlInclude(typeof(System.Text.Json.JsonElement))]
    public class DocRecord
    {
        public string odatatype { get; set; }
        public string odataid { get; set; }
        public string odataetag { get; set; }
        public string odataeditLink { get; set; }
        public int FileSystemObjectType { get; set; }
        public int Id { get; set; }
        public string ServerRedirectedEmbedUri { get; set; }
        public string ServerRedirectedEmbedUrl { get; set; }
        public string ContentTypeId { get; set; }
        public string Title { get; set; }
        public string Document_x0020_Type { get; set; }
        public string Section_x0020_Number { get; set; }
        public object CategoryDescription { get; set; }
        public object CategoryImage { get; set; }
        public object SharedWithUsersId { get; set; }
        public object SharedWithDetails { get; set; }
        public string Subject { get; set; }
        public string Language { get; set; }
        public string Section { get; set; }
        public DateTime Release_x0020_Date { get; set; }
        public object ComplianceAssetId { get; set; }
        public object MediaServiceKeyPoints { get; set; }
        public object OData__ExtendedDescription { get; set; }
        public int ID { get; set; }
        public DateTime Created { get; set; }
        public int AuthorId { get; set; }
        public DateTime Modified { get; set; }
        public int EditorId { get; set; }
        public object OData__CopySource { get; set; }
        public object CheckoutUserId { get; set; }
        public string OData__UIVersionString { get; set; }
        public string GUID { get; set; }
    }
}