﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OfficeOpenXml;
using OfficeOpenXml.LoadFunctions.Params;
using OfficeOpenXml.Table;


namespace TestSharePointOnline
{
    class ExcelOutput
    {
        

        private List<DocRecord> _records;

        public ExcelOutput()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        }

        internal bool CreateWorkbook(string outfn, List<DocRecord> records)
        {
            _records = records;

            if (File.Exists(outfn))
                File.Delete(outfn);

            FileInfo fi = new FileInfo(outfn);

            

            using (var pkg = new ExcelPackage(fi))
            {
                var ws = pkg.Workbook.Worksheets.Add("Summary");

                var t = typeof(DocRecord);

                ws.Cells["A1"].LoadFromCollection(
                    records,
                    true,
                    TableStyles.None,
                    LoadFromCollectionParams.DefaultBindingFlags,
                    new System.Reflection.MemberInfo[]
                    {
                        t.GetProperty("ID"),
                        t.GetProperty("GUID"),
                        t.GetProperty("Subject"),
                        t.GetProperty("Section"),
                        t.GetProperty("Title"),
                        t.GetProperty("Created"),
                        t.GetProperty("ServerRedirectedEmbedUri"),
                        t.GetProperty("Document_x0020_Type")
                    });

                ws.Cells[1, 1, 1, 6].Style.Font.Bold = true;
                pkg.Save();
            }

            return true;
        }
    }
}
